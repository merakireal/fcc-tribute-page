# FCC Tribute Page

![Image of Steve Jobs with some descriptions and quotes](assets/img/tribute-page.png)

Tribute Page dedicated to Steve Jobs. This is a Project Requirement for the [freeCodeCamp Responsive Web Design Certification](https://learn.freecodecamp.org/responsive-web-design/responsive-web-design-projects/build-a-tribute-page). This is built as to practice my CSS Grid skills. See the website: [https://jorenrui.gitlab.io/fcc-tribute-page](https://jorenrui.gitlab.io/fcc-tribute-page).

> This is a makeover of the first [FCC Tribute Page](https://codepen.io/jorenrui/full/qVqPNa/)



## Versions

- **v2** - [GitLab](https://gitlab.com/jorenrui/fcc-tribute-page)
  - CSS Grid
  - Used jQuery and FontAwesome Icons
  - Used FCC Test Suite
- **v1** - [CodePen](https://codepen.io/jorenrui/full/qVqPNa/)
  - Built with Bootstrap 4.
  - Used FontAwesome Icons

![v1](assets/img/v1.png)

## Development Process

### Test Suite

**Technology Stack**

- [x] You can use HTML, JavaScript, and CSS to complete this project. Plain CSS is recommended because that is what the lessons have covered so far and you should get some practice with plain CSS. You can use Bootstrap or SASS if you choose. Additional technologies (just for example jQuery, React, Angular, or Vue) are not recommended for this project, and using them is at your own risk. Other projects will give you a chance to work with different technology stacks like React. We will accept and try to fix all issue reports that use the suggested technology stack for this project. Happy coding!

**Content**

- [x] My tribute page should have an element with corresponding `id="main"`, which contains all other elements.
- [x] I should see an element with corresponding `id="title"`, which contains a string (i.e. text) that describes the subject of the tribute page (e.g. "Dr. Norman Borlaug").
- [x] I should see a `<div>` element with corresponding `id="img-div"`.
- [x] Within the `img-div` element, I should see an `<img>` element with a corresponding `id="image"`.
- [x] Within the `img-div` element, I should see an element with a corresponding `id="img-caption"` that contains textual content describing the image shown in `img-div`.
- [x] I should see an element with a corresponding `id="tribute-info"`, which contains textual content describing the subject of the tribute page.
- [x] I should see an `<a>` element with a corresponding `id="tribute-link"`, which links to an outside site that contains additional information about the subject of the tribute page. HINT: You must give your element an attribute of target and set it to `"_blank"` in order for your link to open in a new tab (i.e. `target="_blank"`).

**Layout**

- [x] The `<img>` element should responsively resize, relative to the width of its parent element, without exceeding its original size.
- [x] The `<img>` element should be centered within its parent element.



### Design

- Traditional Pen and Paper for creating a Layout (Wireframe).
- [Adobe Experience Design](https://www.adobe.com/sea/products/xd.html) for design and prototyping. [See design](designs/tribute-page.xd).

![adobe-xd](assets/img/adobe-xd.png)



### Development

> Note: Added some improvements to the design in the development phase. So slight differences between the design and the final product can be seen.

- Added freeCodeCamp Test Suites
- History:
  1. [CSS Grid Layout](assets/img/app/app1.png)
  2. [Tab Panel Implementation (Cover Section)](assets/img/app/app2.png)
  3. [Tab Panel Content (Cover Section)](assets/img/app/app3.png)
  4. [Added Job Titles and Summary](assets/img/app/app4.png)
  5. [Accordion Implementation](assets/img/app/app5.png)
  6. [Accordion Content](assets/img/app/app6.png)
  7. [End Quote and Footer Section](assets/img/app/app7.png)
  8. [Mobile Responsiveness](assets/img/app/app8.png)



## Mini Codes Used

- [Accordion (CSS-Only)](https://codepen.io/jorenrui/full/mGarKd/)
- [Tabbed Panel](https://codepen.io/jorenrui/pen/NLmqEp)



## References

- https://www.biography.com/people/steve-jobs-9354805
- https://en.wikipedia.org/wiki/Steve_Jobs
- https://allaboutstevejobs.com/bio/short_bio



## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
